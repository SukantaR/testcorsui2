import { Component } from '@angular/core';
import { RequestService } from "./services/request.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [RequestService]
})
export class AppComponent {
  title = 'TestCORSUI - 2';

  constructor(private _request: RequestService) { }

  callingCORSApi() {    
        this._request.callingCORSApi().subscribe(
            (data) => {
                console.log(data);
            }, 
            (error) => { 
                console.log(error) 
            });
    }
}
